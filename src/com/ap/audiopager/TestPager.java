/***************************************************************************
 *   Copyright (C) 2011 by Rynhardt Kruger                                *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package com.ap.audiopager;

public class TestPager {
public static void main(String [] args) throws Exception {
TextPager tp = new TextPager(args[0]);
Marker marker = new Marker(args[0]);
char c = 0;
tp.setPosition(marker.getMark(0));
while (c!='q') {
c = (char) System.in.read();
String res="";
String text = "";
switch(c) {
case 'u':
tp.upLevel();
System.out.println(tp.getLevel());
break;
case 'd':
tp.downLevel();
System.out.println(tp.getLevel());
break;
case 'n':
res = tp.next();
text = tp.current();
System.out.println(res!=null? text:"End");
break;
case 'c':
text = tp.current();
System.out.println(text);
break;
case 'x':
tp.nextChunck();
break;
case 'z':
tp.prevChunck();
break;
case '=':
System.out.println(tp.getPosition());
break;
case 'g':
tp.setPosition(1024*64+20);
break;
case 'p':
res = tp.prev();
text = tp.current();
System.out.println(res!=null? text:"Begin");
break;
case '%':
System.out.println(tp.getPercentage());
tp.setPercentage(94);
}
}
marker.setMark(0,tp.getPosition());
tp.close();
marker.save();
}
}
