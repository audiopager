/***************************************************************************
 *   Copyright (C) 2011 by Rynhardt Kruger                                *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package com.ap.audiopager;

import com.google.marvin.widget.GestureOverlay;
import com.google.marvin.widget.GestureOverlay.GestureListener;
import android.speech.tts.TextToSpeech;

public class AudioSlider {
public interface AudioSliderListener {
public void onItemSelected(int item);
public void onDismissed();
}
GestureOverlay go;
TextToSpeech speaker;
AudioSliderListener cb;
String title;
int value;
int maxVal;

public AudioSlider(GestureOverlay g, TextToSpeech tts, String title, int maxVal, AudioSliderListener cb) {
this.go = g;
this.speaker = tts;
this.title = title;
this.maxVal = maxVal;
this.cb=cb;
value=0;
}

public void show(int item) {
if (value>=maxVal)
return;
value=item;
speaker.speak(title+". "+value,TextToSpeech.QUEUE_FLUSH,null);
go.setGestureListener(new GestureListener() {
public void onGestureStart(int g) {
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
switch(g) {
case 1:
value--;
if (value<0)
value=maxVal-1;
speaker.speak(""+value,TextToSpeech.QUEUE_FLUSH,null);
break;
case 7:
value++;
if (value>=maxVal)
value=0;
speaker.speak(""+value,TextToSpeech.QUEUE_FLUSH,null);
break;
case 2:
value-=5;
if (value < 0)
value+=maxVal;
speaker.speak(""+value,TextToSpeech.QUEUE_FLUSH,null);
break;
case 8:
value+=5;
if (value >=maxVal)
value-=maxVal;
speaker.speak(""+value,TextToSpeech.QUEUE_FLUSH,null);
break;
case 3:
value-=10;
if (value < 0)
value+=maxVal;
speaker.speak(""+value,TextToSpeech.QUEUE_FLUSH,null);
break;
case 9:
value+=10;
if (value>=maxVal)
value-=maxVal;
speaker.speak(""+value,TextToSpeech.QUEUE_FLUSH,null);
break;
case 4:
cb.onDismissed();
break;
case 6:
cb.onItemSelected(value);
break;
case 5:
speaker.speak(""+value,TextToSpeech.QUEUE_FLUSH,null);
break;
}
}
});
}

public void show() {
show(0);
}
}
