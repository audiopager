/***************************************************************************
 *   Copyright (C) 2011 by Rynhardt Kruger                                *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package com.ap.audiopager;

import java.util.*;
import java.io.*;

public class Marker {
long [] marks;
String path;
public Marker(String path) {
this.path=path+".ap";
marks = new long[6];
for (int i = 0; i < marks.length; i++)
marks[i]=0;
load();
}

public void load() {
try {
Scanner sc = new Scanner(new File(path));
for (int i = 0; i < marks.length;i++)
marks[i]=Long.parseLong(sc.nextLine());
sc.close();
} catch(FileNotFoundException x) {
}
}

public void save() {
try {
FileWriter fw = new FileWriter(path);
for (int i = 0; i < marks.length; i++)
fw.write(""+marks[i]+"\n");
fw.close();
} catch(Exception x) {
x.printStackTrace();
}
}

public long getMark(int i) {
if (i>=marks.length)
return 0;
return marks[i];
}

public void setMark(int i,long pos) {
if (i >=marks.length)
return;
marks[i] = pos;
}
}
