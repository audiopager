/***************************************************************************
 *   Copyright (C) 2011 by Rynhardt Kruger                                *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package com.ap.audiopager;

import java.io.*;
import java.util.Stack;

public class TextPager {
char [] data;
String [] levels = {"line","word","character"};
int pos;
int currentLevel;
int chunckNum;
int chunckLength;
RandomAccessFile source;
String path;

public TextPager(String name) {
path=name;
data = new char[16*1024];
try {
source = new RandomAccessFile(name,"r");
} catch(Exception x) {
System.err.println("Error in constructor");
System.exit(1);
}
pos=0;
chunckNum=0;
chunckLength = 0;
nextChunck();
}

public boolean nextChunck() {
try {
char c='\0';
long l = source.getFilePointer();
if (source.getFilePointer()>=source.length())
return false;
int i = 0;
for (i = 0; i < 16*1024&&c>=0; i++) {
if (source.getFilePointer()>=source.length())
break;
c = (char)source.read();
data[i] = c;

// System.out.printf("%d %c\n",i,c);
}

// System.out.println(" "+i);
chunckLength=i;
chunckNum++;
} catch(Exception x) {
System.err.println("Error in nextChunck");
x.printStackTrace();
System.exit(2);
}
return true;
}

public boolean prevChunck() {
try {
if (chunckNum<2)
return false;
chunckNum-=2;
source.seek(1024*16*chunckNum);
return nextChunck();
} catch(Exception x) {
System.err.println("Error in prevChunck");
x.printStackTrace();
System.exit(2);
}
return false;
}

public long getPosition() {
try {
return source.getFilePointer()+pos-(1024*16);
} catch(Exception x) {
return 0;
}
}

public void setPosition(long position) {
pos = (int) position%(1024*16);
try {
source.seek(position-pos);
} catch (Exception x) {
x.printStackTrace();
return;
}
chunckNum=(int)position/(1024*16);
nextChunck();
}

public void setPercentage(int p) {
try {
setPosition((int)(p/100f*source.length()));
} catch (Exception x) {
}
}

public int getPercentage() {
try {
System.out.println(getPosition());
System.out.println(source.length());
return (int)((double)getPosition()/(double)source.length()*100);
} catch(Exception x) {
return 0;
}
}

private boolean nchar() {
if (pos<chunckLength-1) {
pos++;
return true;
}
else if (nextChunck()) {
// System.out.println("baa!");
pos=0;
return true;
}
return false;
}

private boolean pchar() {
if (pos>0) {
pos--;
return true;
} else if (prevChunck()) {
pos=chunckLength-1;
return true;
}
return false;
}

private char cchar() {
return data[pos];
}

private void backToPos(int topos) {
if (topos <= pos)
pos -=topos;
else {
topos = topos-pos;
if (prevChunck())
pos = chunckLength-topos;
else
pos = 0;
}
}

public String readUntil(String u) {
String ret=nextUntil(u);
if (ret!=null)
backToPos(ret.length()+1);
return ret;
}

public void upLevel() {
if (currentLevel>0)
currentLevel--;
else
currentLevel=levels.length-1;
}

public void downLevel() {
currentLevel++;
if (currentLevel >= levels.length)
currentLevel=0;
}

private String nextUntil(String u) {
boolean b=true;
char c=cchar();
b = nchar();
if (!b)
return null;
String ret = "";
if (u.indexOf(c)<0)
ret = ""+c;
while (! oneOf(u)&&b) {
ret+=cchar();
b =nchar();
}
return ret;
}

private String prevUntil(String u) {
boolean b = pchar();
if (!b)
return null;
while (! oneOf(u)&&b) {
b =pchar();
}
return readUntil(u);
}

private String nextParagraph() {
String ret="";
String l = nextUntil('\n');
if (l == null)
return null;
while(l!=null && ! l.equals("")) {
ret+=l+"\n";
l = nextUntil('\n');
}
return ret;
}

private String prevParagraph() {
String l = prevUntil('\n');
if (l==null)
return null;
int num = 0;
if (l.equals(""))
num=1;
while (num<2&&l!=null) {
l = prevUntil('\n');
if (l!=null&&l.equals(""))
num++;
}
if (num>=2)
nchar();
return readParagraph();
}

private void lastSpace() {
if (! oneOf(" \r\n"))
return;
boolean b = nchar();
while (oneOf(" \r\n")&&b)
b = nchar();
}

private void firstSpace() {
if (! oneOf(" \r\n"))
return;
boolean b = pchar();
while (oneOf(" \r\n")&&b)
pchar();
if (! oneOf(" \r\n"))
nchar();
}

private String nextUntil(char u) {
return nextUntil(""+u);
}

private String prevUntil(char u) {
return prevUntil(""+u);
}

private boolean oneOf(String u) {
return u.indexOf(""+cchar())>=0;
}

private String betterName(String name) {
if (name==null)
return name;
if (name.equals(" "))
return "space";
else if (name.equals("\n"))
return "line feed";
else if (name.equals(""))
return "blank";
else
return name;
}

public String next() {
String ret="";
switch(currentLevel) {
case 0:
ret =nextUntil('\n');
ret = betterName(ret);
break;
case 1:
while (ret!=null&&ret.equals(""))
ret = nextUntil(" \r\n");
break;
case 2:
if (nchar())
ret=""+cchar();
else
ret=null;
ret = betterName(ret);
break;
default:
ret="Invalet level";
}
return ret;
}

private String readParagraph() {
String ret = nextParagraph();
if (ret!=null)
backToPos(ret.length()+1);
return ret;
}

public String current() {
String ret="";
switch(currentLevel) {
case 0:
ret =readUntil("\n");
ret = betterName(ret);
break;
case 1:
ret = readUntil(" \r\n");
while (ret!=null&&ret.equals("")) {
nextUntil(" \r\n");
ret = readUntil(" \r\n");
}
break;
case 2:
ret=""+cchar();
ret = betterName(ret);
break;
default:
ret="Invalet level";
}
if (ret==null)
ret="blank"; // safety check
return ret;
}


public String prev() {
String ret="";
switch(currentLevel) {
case 0:
ret=prevUntil('\n');
ret = betterName(ret);
break;
case 1:
while (ret!=null&&ret.equals(""))
ret = prevUntil(" \n\r");
break;
case 2:
if (pchar())
ret = ""+cchar();
else
ret = null;
ret = betterName(ret);
break;
default:
ret="Invalet level";
}
return ret;
}

public String getLevel() {
return levels[currentLevel];
}

public void close() {
try {
source.close();
} catch(Exception x) {
System.exit(3);
}
}

public void goToBegin() {
setPosition(0);
}

public void goToEnd() {
try {
setPosition(source.length()-1);
} catch(Exception x) {
}
}

public void pageDown() {
setLevel(0);
for (int i = 0; i < 25;i++)
if (next()==null)
break;
}

public void pageUp() {
setLevel(0);
for (int i = 0; i < 5; i++)
if (prev() ==null)
break;
}

public void setLevel(int l) {
currentLevel=l;
}

public String getPath() {
return path;
}

public boolean searchForward(String text) {
boolean found = false;
long pos = getPosition();
while (next()!=null&&!found) 
if (current().toLowerCase().indexOf(text.toLowerCase())>=0)
found=true;
if (!found)
setPosition(pos);
return found;
}

public boolean searchBackward(String text) {
boolean found = false;
long pos = getPosition();
while (prev()!=null&&!found)
if (current().toLowerCase().indexOf(text.toLowerCase())>=0)
found=true;
if (!found)
setPosition(pos);
return found;
}
}

