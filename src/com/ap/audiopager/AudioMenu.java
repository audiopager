/***************************************************************************
 *   Copyright (C) 2011 by Rynhardt Kruger                                *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package com.ap.audiopager;

import com.google.marvin.widget.GestureOverlay;
import com.google.marvin.widget.GestureOverlay.GestureListener;
import android.speech.tts.TextToSpeech;

public class AudioMenu {
public interface AudioMenuListener {
public void onItemSelected(int item);
public void onMenuDismissed();
}
GestureOverlay go;
TextToSpeech speaker;
AudioMenuListener cb;
String [] items;
String title;
int index;

public AudioMenu(GestureOverlay g, TextToSpeech tts, String title, String [] items, AudioMenuListener cb) {
this.go = g;
this.speaker = tts;
this.items=items;
this.title = title;
this.cb=cb;
index=0;
}

public void show(int item) {
if (item>=items.length)
return;
index=item;
speaker.speak(title+". "+items[index],TextToSpeech.QUEUE_FLUSH,null);
go.setGestureListener(new GestureListener() {
public void onGestureStart(int g) {
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
switch(g) {
case 2:
index--;
if (index<0)
index=items.length-1;
speaker.speak(items[index],TextToSpeech.QUEUE_FLUSH,null);
break;
case 8:
index++;
if (index>=items.length)
index=0;
speaker.speak(items[index],TextToSpeech.QUEUE_FLUSH,null);
break;
case 4:
cb.onMenuDismissed();
break;
case 6:
cb.onItemSelected(index);
break;
case 5:
speaker.speak(items[index],TextToSpeech.QUEUE_FLUSH,null);
break;
}
}
});
}

public void show() {
show(0);
}
}
