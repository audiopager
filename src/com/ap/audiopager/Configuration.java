package com.ap.audiopager;

import java.util.*;
import java.io.*;

public class Configuration {
HashMap<String,String> settings;
String path;
public Configuration(String path) {
this.path=path;
settings = new HashMap<String,String>();
}

public void load() {
Scanner sc;
try {
sc = new Scanner(new File(path));
} catch(Exception x) {
return;
}
while (sc.hasNext()) {
String line = sc.nextLine();
if (line.length() < 2)
continue;
if (line.indexOf("=")<0)
return;
String []args=line.split("=");
if (args.length<2)
return;
String key=args[0].trim();
String value = args[1].trim();
settings.put(key,value);
}
sc.close();
}

public void save() {
FileWriter setfile;
try {
 setfile= new FileWriter(new File(path));
for (String k : settings.keySet())
setfile.write(k+" = "+settings.get(k)+"\n");
setfile.close();
} catch(Exception x) {
return;
}
}

public String get(String key) {
if (settings.containsKey(key))
return settings.get(key);
else
return null;
}

public void set(String key, String value) {
settings.put(key,value);
}

public static void main(String [] args) {
Configuration conf = new Configuration("test.set");
conf.load();
String name = conf.get("name");
if (name!=null)
System.out.println("name = "+name);
name="Jannie";
conf.set("name",name);
conf.save();
}
}
