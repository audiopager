/***************************************************************************
 *   Copyright (C) 2011 by Rynhardt Kruger                                *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/
 
package com.ap.audiopager;

import android.app.Activity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.*;
import com.google.marvin.widget.GestureOverlay;
import com.google.marvin.widget.GestureOverlay.GestureListener;
import android.widget.FrameLayout;
import android.view.WindowManager;
import java.util.*;
import java.io.File;

public class AudioPagerActivity extends Activity implements GestureListener, Runnable
{
    /** Called when the activity is first created. */
TextToSpeech speaker;
TextPager pager;
GestureOverlay go;
long lastpos;
boolean isReading=false;
Marker marker;
Thread reader = null;
Configuration conf;
String [] menuItems={"open","go to quick bookmark","set quick bookmark", "go to beginning of file", "go to end of file", "Search forward for current text", "search backward for current text","Go to file percentage"};
FileBrowser fb;
AudioPagerActivity self;
boolean running=false;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
self=this;
fb = null;
lastpos=0;
marker = null;
conf=new Configuration("/sdcard/.audiopagerrc");
speaker = new TextToSpeech(this,new OnInitListener() {
public void onInit(int status) {
        setContentView(R.layout.main);
speaker.setSpeechRate(2.5f);
initRest();
}});
    }
public void initRest() {
speaker.speak("Welcome to the audio pager!", 2,null);
conf.load();
String lastbook = conf.get("lastbook");
if (lastbook==null)
pager=null;
else
openFile(lastbook);
go = new GestureOverlay(this,this);
FrameLayout contentFrame = (FrameLayout) findViewById(R.id.contentFrame);
contentFrame.addView(go);
}

public void onDestroy() {
stopReading();
speak("Shutting down...");
autoSave();
while (speaker.isSpeaking());
speaker.shutdown();
super.onDestroy();
}

public void speak(String text) {
speaker.speak(text,2,null);
}

public void showAudioMenu() {
speak("audio menu");
go.setGestureListener(new GestureListener() {
int itm=-1;
public void onGestureStart(int g) {
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
switch(g) {
case 5:
if (itm==-1)
itm=0;
speak(menuItems[itm]);
break;
case 8:
itm++;
if (itm==menuItems.length)
itm=0;
speak(menuItems[itm]);
break;
case 2:
itm--;
if (itm<0)
itm=menuItems.length-1;
speak(menuItems[itm]);
break;
case 6:
selectAudioMenuItem(itm);
break;
default:
speak("Menu dismissed");
go.setGestureListener(self);
}
}
});
}

public void selectAudioMenuItem(int item) {
switch(item) {
case 0:
browseForFile();
break;
case 1:
if (pager==null)
speak("No file loaded");
else
goToMark();
break;
case 2:
if (pager==null)
speak("No file loaded");
else
addMark();
break;
case 3:
if (pager==null)
speak("No file loaded");
else {
pager.goToBegin();
speak(pager.current());
}
go.setGestureListener(self);
break;
case 4:
if (pager==null)
speak("No file loaded");
else {
pager.goToEnd();
speak(pager.current());
}
go.setGestureListener(self);
break;
case 5:
if (pager==null)
speak("No file loaded");
else
if (! pager.searchForward(pager.current()))
speak("Not found");
else
speak(pager.current());
go.setGestureListener(self);
break;
case 6:
if (pager==null)
speak("No file loaded");
else
if (! pager.searchBackward(pager.current()))
speak("not found");
else
speak(pager.current());
go.setGestureListener(self);
break;
case 7:
if (pager==null) {
speak("No file loaded");
go.setGestureListener(self);
} else {
int c= pager.getPercentage();
AudioSlider percentPicker=new AudioSlider(go,speaker,"Select book percentage",100,new AudioSlider.AudioSliderListener() {
public void onItemSelected(int i) {
pager.setPercentage(i);
go.setGestureListener(self);
speak(pager.current());
}
public void onDismissed() {
go.setGestureListener(self);
speak("dismissed");
}
});
percentPicker.show(c);
}
break;
default:
speak("Not implemented");
}
}

public void addMark() {
go.setGestureListener(new GestureListener() {
int m=0;
public void onGestureStart(int g) {
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
switch(g) {
case 2:
m--;
if (m<0)
m=5;
speak(m==0? "auto mark" : "mark " + m);
break;
case 8:
m++;
if (m>5)
m=0;
speak(m==0? "auto mark" : "mark " + m);
break;
case 5:
speak(m==0? "auto mark" : "mark " + m);
break;
case 4:
go.setGestureListener(self);
speak("dismissed");
break;
case 6:
marker.setMark(m,pager.getPosition());
go.setGestureListener(self);
break;
}
}
});
speak("auto mark");
}

public void goToMark() {
go.setGestureListener(new GestureListener() {
int m=0;
public void onGestureStart(int g) {
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
switch(g) {
case 2:
m--;
if (m<0)
m=5;
speak(m==0? "auto mark" : "mark " + m);
break;
case 8:
m++;
if (m>5)
m=0;
speak(m==0? "auto mark" : "mark " + m);
break;
case 5:
speak(m==0? "auto mark" : "mark " + m);
break;
case 4:
go.setGestureListener(self);
speak("dismissed");
break;
case 6:
pager.setPosition(marker.getMark(m));
go.setGestureListener(self);
break;
}
}
});
speak("auto mark");
}

public void browseForFile() {
autoSave();
speak("File browser");
fb = new FileBrowser("/sdcard/ebooks", new FileBrowser.FileSelectedListener() {
public void onCanceled() {
speak("canceled");
go.setGestureListener(self);
}

public void onFileSelected(String path) {
go.setGestureListener(self);
conf.set("lastbook",path);
openFile(path);
}
});
go.setGestureListener(new GestureListener() {
public void onGestureStart(int g) {
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
switch(g) {
case 5:
speak(fb.current());
break;
case 3:
fb.prev20();
speak(fb.current());
break;
case 9:
fb.next20();
speak(fb.current());
break;
case 1:
fb.first();
speak(fb.current());
break;
case 7:
fb.last();
speak(fb.current());
break;
case 8:
fb.next();
speak(fb.current());
break;
case 2:
fb.prev();
speak(fb.current());
break;
case 6:
fb.down();
break;
case 4:
fb.up();
speak(fb.current());
break;
}
}
});
speak(fb.current());
}

public void onResume() {
super.onResume();

}

public void onPause() {
super.onPause();
stopReading();
autoSave();
speak("saving");
}

public void autoLoad() {
if (pager==null)
return;
marker = new Marker(pager.getPath());
pager.setPosition(marker.getMark(0));
}

public void autoSave() {
if (pager==null)
return;
marker.setMark(0,pager.getPosition());
marker.save();
conf.save();
}

public void run() {
String b="";
pager.setLevel(0);
while (isReading) {
String text="";
lastpos = pager.getPosition();
// skip over blanks
while (pager.current().equals("blank"))
if (pager.next()==null)
break;
for (int i = 0; i < 12&&! pager.current().equals("blank");i++) {
text +=pager.current()+" ";
b =pager.next();
if (pager.current().length()>80)
break; // we assume that the hole paragraph is on one line
}
if (b==null)
isReading=false;
speak(text);
while (speaker.isSpeaking())
try {
reader.sleep(100);
} catch(Exception x) {
}
}
}

public void speakYes() {
if (speaker.isSpeaking())
speaker.speak("yeah",2,null);
}

public void startReading() {
if (isReading)
return;
isReading = true;
reader = new Thread(this);
go.setGestureListener(new GestureListener() {
int [] gesture = {5,5};
int counter = 0;
public void onGestureStart(int g) {
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
if (g == gesture[counter])
counter++;
else
counter=0;
if (counter>=gesture.length) {
go.setGestureListener(self);
stopReading();
}
}
});
reader.start();
}

public void stopReading() {
if (reader==null)
return;
isReading = false;
speaker.stop();
reader.stop();
reader = null;
pager.setPosition(lastpos);
}

public void onGestureStart(int g) {
speaker.stop();
}

public void onGestureChange(int g) {
}

public void onGestureFinish(int g) {
switch(g) {
case 6:
if (pager==null)
return;
if (pager.next()!=null)
speaker.speak(pager.current(),2,null);
else
speaker.speak("end",2,null);
break;
case 4:
if (pager==null)
return;
if (pager.prev()!=null)
speaker.speak(pager.current(),2,null);
else
speaker.speak("end",2,null);
break;
case 2:
if (pager==null)
return;
pager.upLevel();
speaker.speak(pager.getLevel(),2,null);
break;
case 8:
if (pager==null)
return;
pager.downLevel();
speaker.speak(pager.getLevel(),2,null);
break;
case 5:
if (pager==null)
speak("No file loaded!");
else
stopReading();
break;
case 9:
if (pager==null)
return;
startReading();
break;
case 1:
if (pager==null)
speak("no file loaded");
else {
speak("page up");
pager.pageUp();
speak(pager.current());
}
break;
case 3:
if (pager==null)
speak("no file loaded");
else {
speak("page down");
pager.pageDown();
speak(pager.current());
}
break;
case 7:
showAudioMenu();
break;
}
}

public void openFile(String path) {
if (! new File(path).exists())
return;
pager = new TextPager(path);
autoLoad();
speak(pager.current());
}
}
