/***************************************************************************
 *   Copyright (C) 2011 by Rynhardt Kruger                                *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package com.ap.audiopager;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Stack;
import java.util.Arrays;

public class FileBrowser {
Stack<File> levels;
File [] files;
int currentFile=0;
FileSelectedListener fl;

public interface FileSelectedListener {
public void onFileSelected(String path);
public void onCanceled();
}

public FileBrowser(String path, FileSelectedListener fl) {
this.fl=fl;
levels=new Stack<File>();
levels.push(new File(path));
list();
}

public void list() {
currentFile=0;
files = levels.peek().listFiles(new FilenameFilter() {
public boolean accept(File dir, String name) {
return ! name.endsWith(".ap");
}
});
Arrays.sort(files);
}

public String current() {
if (files.length==0)
return "No files";
else
return files[currentFile].getName();
}

public void next() {
currentFile++;
if (currentFile==files.length)
currentFile=0;
}

public void prev() {
currentFile--;
if (currentFile<0)
currentFile=Math.max(files.length-1,0);
}

public void down() {
if (files[currentFile].isFile())
fl.onFileSelected(files[currentFile].getPath());
else if (files[currentFile].isDirectory()) {
levels.push(files[currentFile]);
list();
}
}

public void up() {
levels.pop();
if (levels.isEmpty())
fl.onCanceled();
else {
list();
}
}

public void next20() {
if (currentFile +20 < files.length)
currentFile+=20;
}

public void prev20() {
if (currentFile >=20)
currentFile-=20;
}

public void first() {
currentFile=0;
}

public void last() {
if (files.length>0)
currentFile=files.length-1;
}
}
